# Getting Started

To start the project run,

``` mvn spring-boot:run```

If you do a plain curl to the service, e.g.
```
curl http://localhost:8080/hello
```
you'll get an HTTP 401 Unauthorized.

To be able use the service, you first need to retrieve the JWT and attach it as a header. Use the following curl to obtain the JWT.
```
curl -d '{"username": "anyone", "password": "password"}' -H "Content-Type: application/json" -X POST localhost:8080/authenticate
```
This will return for example,
```
{"token":"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNTY1NzA4NzUzLCJpYXQiOjE1NjU2OTA3NTN9.ZTs59HvZeLDSFV-WQ2WrhMyDv-57ZIxcUiNLV6qNWvyYvtSmTgtXq0WwkO4StM9DmL0YzP9f_4BRg96L0T7nOQ"}
```
Using the above example token, you can then call the hello endpoint as below.
```
curl http://localhost:8080/hello -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNTY1NzA4NzUzLCJpYXQiOjE1NjU2OTA3NTN9.ZTs59HvZeLDSFV-WQ2WrhMyDv-57ZIxcUiNLV6qNWvyYvtSmTgtXq0WwkO4StM9DmL0YzP9f_4BRg96L0T7nOQ'
```
You will need to replace the JWT with the one you got back from the `/authenticate` call.

To test the SQL exception endpoint, you can call,
```
curl http://localhost:8080/sqlexception -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNTY1NzA4NzUzLCJpYXQiOjE1NjU2OTA3NTN9.ZTs59HvZeLDSFV-WQ2WrhMyDv-57ZIxcUiNLV6qNWvyYvtSmTgtXq0WwkO4StM9DmL0YzP9f_4BRg96L0T7nOQ'
```
You should get a 500 Internal Server Error that shows the exception - this is how Spring handles uncaught exceptions - other Java frameworks will treat them differently.

And to call the endpoint that manually catches the exception, you can call,
```
curl http://localhost:8080/exceptioncaught -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiZXhwIjoxNTY1NzA4NzUzLCJpYXQiOjE1NjU2OTA3NTN9.ZTs59HvZeLDSFV-WQ2WrhMyDv-57ZIxcUiNLV6qNWvyYvtSmTgtXq0WwkO4StM9DmL0YzP9f_4BRg96L0T7nOQ'
```
