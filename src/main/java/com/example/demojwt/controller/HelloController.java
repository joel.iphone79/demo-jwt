package com.example.demojwt.controller;

import com.example.demojwt.service.FakeSqlExceptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;

@RestController
public class HelloController {

    private final FakeSqlExceptionService fakeSqlExceptionService;

    @Autowired
    public HelloController(FakeSqlExceptionService fakeSqlExceptionService) {
        this.fakeSqlExceptionService = fakeSqlExceptionService;
    }

    @RequestMapping({ "/hello" })
    public String hello() {
        return "Hello World";
    }

    @RequestMapping({ "/sqlexception" })
    public String sqlException() throws SQLException {
        return fakeSqlExceptionService.getData();
    }

    @RequestMapping({ "/exceptioncaught" })
    public String exceptionCaught() {
        try {
            return fakeSqlExceptionService.getData();
        } catch (SQLException e) {
            return "Exception caught - doing something else";
        }
    }
}
