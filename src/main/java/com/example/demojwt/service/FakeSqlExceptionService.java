package com.example.demojwt.service;

import org.springframework.stereotype.Service;

import java.sql.SQLException;

@Service
public class FakeSqlExceptionService {

    public String getData() throws SQLException {
        throw new SQLException("Some really bad exception");
    }

}
